def slidingWindow(sequence,winSize,step=1):
    """Returns a generator that will iterate through
    the defined chunks of input sequence.  Input sequence
    must be iterable."""

    # Verify the inputs
    try: it = iter(sequence)
    except TypeError:
        raise Exception("**ERROR** sequence must be iterable.")
    if not ((type(winSize) == type(0)) and (type(step) == type(0))):
        raise Exception("**ERROR** type(winSize) and type(step) must be int.")
    if step > winSize:
        raise Exception("**ERROR** step must not be larger than winSize.")
    if winSize > len(sequence):
        raise Exception("**ERROR** winSize must not be larger than sequence length.")
 
    # Pre-compute number of chunks to emit
    numOfChunks = ((len(sequence)-winSize)/step)+1
 
    # Do the work
    for i in range(0,numOfChunks*step,step):
        #yield sequence[i:i+winSize]
        print sequence[i:i+winSize]

if __name__ == '__main__':
    lines_matrix = [line.strip() for line in open("in.txt")]
    lines_matrix = [line.split() for line in lines_matrix]
    words_matrix = [item for sublist in lines_matrix for item in sublist]
    words_matrix = [x.lower() for x in words_matrix]
    slidingWindow(words_matrix,3)