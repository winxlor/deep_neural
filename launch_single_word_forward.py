import numpy, argparse, os, re, random, math
import numpy as np
from collections import Counter

def sigmoid(x):
	print "Hello",x
	return 1 / (1 + math.exp(-x))
def slidingWindow(sequence,winSize,step=1):
	print "\n\nIn slidingWindow, the value of W is"
	global W
	print W
	"""Returns a generator that will iterate through
	the defined chunks of input sequence.  Input sequence
	must be iterable."""

	# Verify the inputs
	try: it = iter(sequence)
	except TypeError:
		raise Exception("**ERROR** sequence must be iterable.")
	if not ((type(winSize) == type(0)) and (type(step) == type(0))):
		raise Exception("**ERROR** type(winSize) and type(step) must be int.")
	if step > winSize:
		raise Exception("**ERROR** step must not be larger than winSize.")
	if winSize > len(sequence):
		raise Exception("**ERROR** winSize must not be larger than sequence length.")

	# Pre-compute number of chunks to emit
	numOfChunks = ((len(sequence)-winSize)/step)+1

	# Do the work
	for i in range(0,numOfChunks*step,step):
		#yield sequence[i:i+winSize]
		#print sequence[i:i+winSize]
		#print vector_update[[x for x in sequence[i:i+winSize]]]
		current_window = [x for x in sequence[i:i+winSize]]
		print "This is the current window :"
		print current_window
		#print "And the line is now to be converted into a better representation"
		#print (lambda x : vector_update[x] x in sequence[i:i+winSize])[x for x in sequence[i:i+winSize]]
		#print [lambda x: vector_update[x] for x in [x1 for x1 in sequence[i:i+winSize]]]
		#print [vector_update[x] for x in [x1 for x1 in sequence[i:i+winSize]]]
		abc = [vector_update[x] for x in [x1 for x1 in sequence[i:i+winSize]]]
		print abc
		'''
		abc = np.array(abc)
		print abc
		return
		'''
		abc = np.asarray(abc)
		#print abc
		abc = np.concatenate(abc)
		abc.resize(len(abc),1)
		#print abc
		if(args.window_size%2==0):
			center = args.window_size/2
		else:
			center = (args.window_size-1)/2
		'''
		print "The window_size is ",
		print args.window_size
		print "The center's value is ",
		print center,
		print "which holds the value '",
		print current_window[center],
		print "'"
		'''
		for word_tuple in words:
			if(word_tuple[0]!=current_window[center]):
				print word_tuple[0],
				print "+"
				print "The word's representation is :",vector_update[word_tuple[0]]
				W = np.asmatrix(W)
				print "W is "
				print W
				print type(W)
				abc = np.asmatrix(abc)
				print "abc has been converted"
				print type(abc)
				print abc
				#Z = np.multiply(W,abc)
				Z = W*abc
				print "The first multiplication is done"
				print Z
				#np.set_printoptions(precision=40)
				A = np.tanh(Z)
				print "tanh function has been acted upon"
				print A
				#print np.tanh(0.91742687)
				global W_layer2
				print "W_layer2 is of shape :",W_layer2.shape
				print "A is of shape :",A.shape
				Z_2 = (W_layer2)*A
				print "Z_2 is"
				print Z_2
				#Z_2 = (W_layer2.transpose())*A
				h_w = sigmoid(Z_2)
				print "h_w(x) is "
				print h_w
				#W_l2_temp=W_layer2.transpose()
				#Z_2 = np.multiply(W_l2_temp,np.tanh(W_l2_temp))
				#Z_2 = np.multiply(W_l2_temp,np.tanh(W_l2_temp))
				#print Z_2
				#np.tanh((0, np.pi*1j, np.pi*1j/2))
				break
				#Comment this out when the random word sampling has to be be carried out for the entire set
			else:
				continue
		'''
		print ""
		print "abc is "
		print abc
		'''
		'''
		multiply_matrix = np.asarray([2])
		print "[2]"
		'''

		'''
		multiply_matrix = np.asarray([[2]])
		#print "[[2]]"
		
		#multiply_matrix = np.asarray([[1],[2]])
		multiply_matrix = multiply_matrix.reshape((1,1))
		print "Multiplication matrix is "
		print multiply_matrix
		print np.multiply(abc,multiply_matrix)
		'''
		break
		#Comment this out when the sliding window has to be carried out for the entire sequence

def word_vocab_build():
	#global lines
	global words
	lines = [line.strip() for line in open(args.input_file)]
	lines = [line.split() for line in lines]
	words = [item for sublist in lines for item in sublist]
	if(not args.uppercase):
		words = [x.lower() for x in words]
	words = Counter(words).most_common(len(words))
	statinfo = (os.stat(args.input_file).st_size)/(1024**2)
	print "The input file is of size :",statinfo
	if(statinfo<10):
		print words
	file_output = open(args.vocabulary, 'w')
	file_output_count = open(args.vocabulary_count, 'w')
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			#continue
			break
		else:
			file_output.write(word_tuple[0]+os.linesep)
			file_output_count.write(word_tuple[0]+"\t"+str(word_tuple[1])+os.linesep)

def word_vec_init():
	#print [random.randrange(1, 10) for _ in range(0, args.window_size)]
	#vec_init = [random.randrange(1, 10) for _ in range(0, 4)]
	global vec_init
	vec_init = [random.uniform(1, 10) for _ in range(0, args.window_size)]
	#print len(vec_init)
	file_vec_output = open(args.vector_output, 'w')
	file_vec_output_word = open("word_vector_output_verbose.txt", 'w')
	global vector_update
	vector_update = {}
	vec_dummy = [1]*args.word_size
	#vector_update["DGDD"] = ' '.join(str(e) for e in vec_dummy)
	vector_update["DGDD"] = vec_dummy
	print "DGDD and : ",
	print vector_update["DGDD"]
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			break
		else:
			#vec_init = [random.uniform(1, 10) for _ in range(0, args.window_size)]
			#vec_init = [random.uniform(1, 10) for _ in range(0, 5)]
			vec_init = [random.randrange(1, 10) for _ in range(0, args.word_size)]
			vec_init_str=' '.join(str(e) for e in vec_init)
			#vector_update[word_tuple[0]] = vec_init_str
			vector_update[word_tuple[0]] = vec_init
			#'''
			file_vec_output.write(vec_init_str+os.linesep)
			file_vec_output_word.write(word_tuple[0]+"\t"+vec_init_str+os.linesep)
			#'''
def word_vec_check():
	print "\n\nIn word_vec_check, the value of W is"
	global W
	print W
	return
	lines_matrix = [line.strip() for line in open(args.input_file)]
	lines_matrix = [line.split() for line in lines_matrix]
	words_matrix = [item for sublist in lines_matrix for item in sublist]
	if(not args.uppercase):
		words_matrix = [x.lower() for x in words_matrix]
	for word_matrix in words_matrix:
		print word_matrix + " : " + vector_update[word_matrix]

def word_vec_process():
	print "\n\nIn word_vec_process, the value of W is"
	global W
	print W
	print "Initial checking : vector_update[\"this : \"]",
	#print type(vector_update["this"])
	print type(vector_update["this"][0])
	word_vec_check()
	print "Hello"
	lines_matrix = [line.strip() for line in open(args.input_file)]
	lines_matrix = [line.split() for line in lines_matrix]
	print "Hello 1"
	#print lines_matrix
	for lines_matrix_each in lines_matrix:
		#print lines_matrix_each
		lines_matrix_each = [line.split() for line in lines_matrix_each]
		words_matrix = [item for sublist in lines_matrix_each for item in sublist]
		if(not args.uppercase):
			words_matrix = [x.lower() for x in words_matrix]
		print "We are sending : "
		print words_matrix
		#print lines_matrix_each
		#print "The line is now to be processed"
		padding = 3-2;
		words_matrix = ["DGDD"]*padding + words_matrix + ["DGDD"]*padding
		slidingWindow(words_matrix,args.window_size)
		return
		#Remove the preceding line for complete functioning
	return
	lines_matrix = [line.split() for line in lines_matrix]
	words_matrix = [item for sublist in lines_matrix for item in sublist]
	if(not args.uppercase):
		words_matrix = [x.lower() for x in words_matrix]
	print words_matrix
	print "The sliding window"
	slidingWindow(words_matrix, args.window_size)
	'''
	lines_matrix = [line.strip() for line in open(args.input_file)]
	lines_matrix = [line.split() for line in lines_matrix]
	words_matrix = [item for sublist in lines_matrix for item in sublist]
	if(not args.uppercase):
		words_matrix = [x.lower() for x in words_matrix]
	for word_matrix in words_matrix:
		print word_matrix + " : " + vector_update[word_matrix]
	'''

def weight_init():
	print "Init"
	global W
	global W_layer2
	W = numpy.random.uniform(low=0, high=10, size=(args.deep_neural,args.word_size*args.window_size))
	W_layer2 = numpy.random.uniform(low=0, high=10, size=(1,args.deep_neural))
	#A = numpy.random.uniform(low=0.75, high=1.5, size=(2,3) )
	#W = numpy.random.rand(args.deep_neural,args.word_size*args.window_size)
	print "The hidden neural is :",args.deep_neural
	print "The word vector size is :",args.word_size
	print "The window size is :",args.window_size
	#print "W = numpy.random.rand(args.deep_neural,args.word_size*args.window_size)"
	print "W = numpy.random.uniform(low=0, high=10, size=(args.deep_neural,args.word_size*args.window_size))"
	print "The weight matrix is :"
	#print W
	print W.shape
	print "W is"
	print W

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-square", help="display the square of a given number", type=int, default=2)
	parser.add_argument("-dn", "--deep-neural", help="Deep Neural Network size", type=int, default=50)
	parser.add_argument("-window", "--window-size", help="Window size of words(5)", type=int, default=5)
	parser.add_argument("-word", "--word-size", help="Word vector size(50)", type=int, default=50)
	parser.add_argument("-iter", "--iteration-count", help="iteration count upperbound", type=int, default=5000)
	parser.add_argument("-eps", "--epsilon", help="epsilon value for termination condition", type=int, default=0.001)
	parser.add_argument("-in", "--input-file", help="corpus to read from in order to create the word vector", default="in.txt")
	parser.add_argument("-vocab", "--vocabulary", help="Save the vocabulary file to entered location", default="build_vocab.txt")
	parser.add_argument("-vocabc", "--vocabulary-count", help="Save the vocabulary file with the frequencies to entered location", default="build_vocab_count.txt")
	parser.add_argument("-vec", "--vector-output", help="Save the word vectors to the given file", default="word_vector_output.txt")
	parser.add_argument("-uc", "--uppercase", help="Let words remain in uppercase", action="store_true")
	parser.add_argument("-wlimit", "--word-limit", help="Set a lower bound for word frequencies", type=int, default=0)
	parser.add_argument("-v", "--verbosity", help="increase output verbosity", action="store_true")
	args = parser.parse_args()
	answer = args.square**2
	weight_init()
	''''
	print "The values are : "
	print "Deep Neural : ", args.deep_neural
	print "Window Size : ", args.window_size
	print "Word Size : ", args.word_size
	print "Iteration Count : ", args.iteration_count
	print "Epsilon : ", args.epsilon
	print "Input File : ", args.input_file
	print "Vocabulary File : ", args.vocabulary
	print "Vocabulary Count File : ", args.vocabulary_count
	print "Uppercase : ", args.uppercase
	print "Word Limit : ", args.word_limit
	print "Vector File : ", args.vector_output
	print "Verbosity : ", args.verbosity
	'''
	'''
	if(args.verbosity):
		#print "Verbosity is turned on"
		print "The square of {} equals {}".format(args.square, answer)
	else:
		print answer
	'''
	word_vocab_build()
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			#continue
			break
		else:
			#print word_tuple[0]
			#print word_tuple[0]+"\t"+str(word_tuple[1])
			#a=1
			continue
	word_vec_init()
	print "\n\nIn main, the value of W is"
	global W
	print W
	word_vec_process()