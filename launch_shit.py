import numpy, argparse, os, re, random
from collections import Counter
'''
parser = argparse.ArgumentParser(description='Configuration for the deep neural network')
parser.add_argument('integers', metavar='N', type=int, nargs='+',help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const', const=sum, default=max, help='sum the integers (default: find the max)')
args = parser.parse_args()
print(args.accumulate(args.integers))

Deep Neural size default is 50
Word size default is 5
Termination count default is 5000
Epsilon default is 0.001
'''
def word_vocab_build():
	'''
	print "The values are : "
	print "Deep Neural : ", args.deep_neural
	print "Window Size : ", args.window_size
	print "Word Size : ", args.word_size
	print "Iteration Count : ", args.iteration_count
	print "Epsilon : ", args.epsilon
	print "Input File : ", args.input_file
	print "Vocabulary File : ", args.vocabulary
	print "Vocabulary Count File : ", args.vocabulary_count
	print "Uppercase : ", args.uppercase
	print "Word Limit : ", args.word_limit
	print "Vector File : ", args.vector_output
	print "Verbosity : ", args.verbosity
	'''
	#global lines
	global words
	'''
	with open('in.txt','r') as f:
		for line in f:
			#print line
			continue
			for word in line.split():
				#print(word)
	'''
	lines = [line.strip() for line in open(args.input_file)]
	lines = [line.split() for line in lines]
	words = [item for sublist in lines for item in sublist]
	if(not args.uppercase):
		words = [x.lower() for x in words]
	words = Counter(words).most_common(len(words))
	statinfo = (os.stat(args.input_file).st_size)/(1024**2)
	print "The input file is of size :",statinfo
	if(statinfo<10):
		print words
	file_output = open(args.vocabulary, 'w')
	file_output_count = open(args.vocabulary_count, 'w')
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			#continue
			break
		else:
			file_output.write(word_tuple[0]+os.linesep)
			file_output_count.write(word_tuple[0]+"\t"+str(word_tuple[1])+os.linesep)

def word_vec_init():
	#print [random.randrange(1, 10) for _ in range(0, args.window_size)]
	#vec_init = [random.randrange(1, 10) for _ in range(0, 4)]
	global vec_init
	vec_init = [random.uniform(1, 10) for _ in range(0, args.window_size)]
	#print len(vec_init)
	file_vec_output = open(args.vector_output, 'w')
	file_vec_output_word = open("word_vector_output_verbose.txt", 'w')
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			break
		else:
			vec_init = [random.uniform(1, 10) for _ in range(0, args.window_size)]
			vec_init_str=' '.join(str(e) for e in vec_init)
			file_vec_output.write(vec_init_str+os.linesep)
			file_vec_output_word.write(word_tuple[0]+"\t"+vec_init_str+os.linesep)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-square", help="display the square of a given number", type=int, default=2)
	parser.add_argument("-dn", "--deep-neural", help="Deep Neural Network size", type=int, default=50)
	parser.add_argument("-window", "--window-size", help="Word vector size", type=int, default=50)
	parser.add_argument("-word", "--word-size", help="Word vector size", type=int, default=5)
	parser.add_argument("-iter", "--iteration-count", help="iteration count upperbound", type=int, default=5000)
	parser.add_argument("-eps", "--epsilon", help="epsilon value for termination condition", type=int, default=0.001)
	parser.add_argument("-in", "--input-file", help="corpus to read from in order to create the word vector", default="in.txt")
	parser.add_argument("-vocab", "--vocabulary", help="Save the vocabulary file to entered location", default="build_vocab.txt")
	parser.add_argument("-vocabc", "--vocabulary-count", help="Save the vocabulary file with the frequencies to entered location", default="build_vocab_count.txt")
	parser.add_argument("-vec", "--vector-output", help="Save the word vectors to the given file", default="word_vector_output.txt")
	parser.add_argument("-lc", "--uppercase", help="Use everything in lowercase", action="store_true")
	parser.add_argument("-wlimit", "--word-limit", help="Set a lower bound for word frequencies", type=int, default=0)
	parser.add_argument("-v", "--verbosity", help="increase output verbosity", action="store_true")
	args = parser.parse_args()
	answer = args.square**2
	print "The values are : "
	print "Deep Neural : ", args.deep_neural
	print "Window Size : ", args.window_size
	print "Word Size : ", args.word_size
	print "Iteration Count : ", args.iteration_count
	print "Epsilon : ", args.epsilon
	print "Input File : ", args.input_file
	print "Vocabulary File : ", args.vocabulary
	print "Vocabulary Count File : ", args.vocabulary_count
	print "Uppercase : ", args.uppercase
	print "Word Limit : ", args.word_limit
	print "Vector File : ", args.vector_output
	print "Verbosity : ", args.verbosity
	if(args.verbosity):
		#print "Verbosity is turned on"
		print "The square of {} equals {}".format(args.square, answer)
	else:
		print answer
	word_vocab_build()
	for word_tuple in words:
		if(word_tuple[1]<args.word_limit):
			#continue
			break
		else:
			#print word_tuple[0]
			#print word_tuple[0]+"\t"+str(word_tuple[1])
			a=1
	word_vec_init()
	'''
	for line in lines:
		for word in line:
			vocab1.append(word)
			if(not args.uppercase):
				word = word.lower()
			if(not(word in vocab)):
				vocab.append(word)
			else:
				#print word, "is repeated"
				a=1
			#print word
	#print vocab
	#print vocab1
	#words = re.findall(r'\w+', open('in.txt').read().lower())

	print words
	print Counter(words)
	file_output = open(args.vocabulary, 'w')
	for word in vocab:
		file_output.write(word+os.linesep)
	'''